const initialState = {
  articles: [],
  article: {},
  isLoading: false,
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case 'GET_ARTICLES_REQUEST': return {
      ...state,
      isLoading: true,
      error: null,
    };
    case 'GET_ARTICLES_SUCCESS': return {
      ...state,
      isLoading: false,
      articles: action.payload.articles,
    };
    case 'GET_ARTICLES_FAILURE': return {
      ...state,
      isLoading: false,
      error: null,
    };
    case 'GET_ARTICLE_REQUEST': return {
      ...state,
      isLoading: true,
      error: null,
    };
    case 'GET_ARTICLE_SUCCESS': return {
      ...state,
      isLoading: false,
      article: action.payload.article,
    };
    case 'GET_ARTICLE_FAILURE': return {
      ...state,
      isLoading: false,
      error: null,
    };
    default: return state;
  }
};

