import * as req from '../../data/articles';

export function getArticles() {
  return (dispatch) => {
    dispatch({ type: 'GET_ARTICLES_REQUEST' });

    return req.GetArticles().then((resp) => {
      dispatch({
        type: 'GET_ARTICLES_SUCCESS',
        payload: {
          articles: resp.data,
        },
      });
    }).catch((err) => {
      console.error(err);
      dispatch({
        type: 'GET_ARTICLES_FAILURE',
        payload: err,
      });
    });
  };
}

export function getArticle() {
  return (dispatch) => {
    dispatch({ type: 'GET_ARTICLE_REQUEST' });

    return req.GetArticle().then((resp) => {
      dispatch({
        type: 'GET_ARTICLE_SUCCESS',
        payload: {
          article: resp.data,
        },
      });
    }).catch((err) => {
      console.error(err);
      dispatch({
        type: 'GET_ARTICLE_FAILURE',
        payload: err,
      });
    });
  };
}

