import React from 'react';
import { HashRouter } from 'react-router-dom';
import Routes from '../src/routes'
class App extends React.Component {
  render() {
    return (
      <HashRouter>
        <Routes />
      </HashRouter>
    );
  }
}

export default App;
