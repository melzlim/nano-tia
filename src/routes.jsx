import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { withCookies } from 'react-cookie';
import App from './scenes/App';
import Home from '../src/scenes/Home';
import Article from './scenes/Article';


class Routes extends React.Component {
  render() {
    return (
      <App>
        <Switch>
          <Route
            exact path="/"
            render={() => (<Home cookies={this.props.cookies} />)}
          />
          <Route
            exact path="/:article_name"
            render={() => (<Article cookies={this.props.cookies} />)}
          />
        </Switch>
      </App>
    );
  }
}

export default withCookies(Routes);
