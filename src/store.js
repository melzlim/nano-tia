import { combineReducers } from 'redux';
import Articles from './services/articles/reducers';

export default combineReducers({
  Articles,
});
