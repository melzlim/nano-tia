import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';
import Loader from '../../components/Loader';
import { getArticles } from '../../services/articles/actions';

class Home extends React.Component {
  componentDidMount() {
    this.props.getArticles();
    const { cookies } = this.props;
    const cookie = cookies.get('articlesRead');
    const expiryTime = this.getExpiryTime();

    if (isEmpty(cookie)) {
      cookies.set('articlesRead', 0, { path: '/', expires: expiryTime });
    }
  }

  getExpiryTime = () => {
    const now = new Date();
    if (now.getMonth() === 11) {
      const current = new Date(now.getFullYear() + 1, 0, 1);
      return current;
    } else {
      const current = new Date(now.getFullYear(), now.getMonth() + 1, 1);
      return current;
    }
  }

  redirectToArticle = () => {
    const { cookies } = this.props;
    const cookie = cookies.get('articlesRead');
    const expiryTime = this.getExpiryTime();

    if (isEmpty(cookie)) {
      cookies.set('articlesRead', 1, { path: '/', expires: expiryTime });
    } else {
      let count = parseInt(cookies.get('articlesRead'));
      count = count + 1;
      cookies.set('articlesRead', count, { path: '/', expires: expiryTime });
    }
  }

  render() {
    const { posts } = this.props.articles.articles;
    const { isLoading } = this.props.articles
    return (
      <div className='container'>
        {!isLoading ? (
          <React.Fragment>
            {!isEmpty(posts) ? (
              <React.Fragment>
                {posts.map(post => (
                  <div key={post.id} className='row mt-5'>
                    <div className='col-lg-4 col-sm-12'>
                      <Link
                        to={`${post.slug}`}
                        onClick={this.redirectToArticle}
                      >
                        <img
                          src={post['featured_image']['source']}
                          alt=''
                          style={{ width: '100%', height: '100%' }}
                        />
                      </Link>
                    </div>
                    <div className='col-lg-6 col-sm-12'>
                      <Link
                        to={`${post.slug}`}
                        style={{ textDecoration: 'none', color: '#000000' }}
                        onClick={this.redirectToArticle}
                      >
                        <h3 style={{ fontWeight: 'bold' }}>{post.title}</h3>
                      </Link>
                      <p className='mt-4'>{post.excerpt}</p>
                    </div>
                  </div>
                ))}
              </React.Fragment>
            ) : (
                <div className='row mt-5'>
                  <div className='col-12' style={{ textAlign: 'center' }}>
                    Error Loading Articles. Please try again
                  </div>
                </div>
              )}
          </React.Fragment>
        ) : (
            <Loader
              style={{ width: '100%', marginTop: 300 }}
              noText
            />
          )}

      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  articles: state.Articles,
  cookies: ownProps.cookies,
});

const mapDispatchToProps = {
  getArticles,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
