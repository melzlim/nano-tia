import React, { Fragment } from 'react';
import { withRouter } from 'react-router-dom';
import Header from '../../components/Header';
class App extends React.Component {


  render() {
    return (
      <Fragment>
        <Header />
        {this.props.children}
      </Fragment>
    );
  }
}

export default withRouter(App);
