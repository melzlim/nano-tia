import React from 'react';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';
import Parser from 'html-react-parser';
import trimHtml from 'trim-html';
import { getArticle } from '../../services/articles/actions';
import styles from './styles';

class Article extends React.Component {
  componentDidMount() {
    this.props.getArticle()
    const { cookies } = this.props;
    const cookie = cookies.get('articlesRead');
    const expiryTime = this.getExpiryTime();

    if (isEmpty(cookie)) {
      cookies.set('articlesRead', 1, { path: '/', expires: expiryTime });
    }
    this.articlesRead = cookies.get('articlesRead');
  }

  getExpiryTime = () => {
    const now = new Date();
    if (now.getMonth() === 11) {
      const current = new Date(now.getFullYear() + 1, 0, 1);
      return current;
    } else {
      const current = new Date(now.getFullYear(), now.getMonth() + 1, 1);
      return current;
    }
  }

  render() {
    const { posts } = this.props.articles.article;
    return (
      <div
        className='container'
        style={{ fontFamily: 'Roboto' }}
      >
        {!isEmpty(posts) ? (
          <React.Fragment>
            <div className='row' style={{ marginBottom: 10 }}>
              <div className='col-12'>
                {posts[0].categories.map(category => (
                  <span
                    key={category.id}
                    style={{ textTransform: 'uppercase', fontSize: 13 }}>
                    {category.name} /
                  </span>
                ))}
              </div>
            </div>
            <div className='row'>
              <div className='col-12'>
                <img
                  src={posts[0].author['avatar_url']} alt='profile'
                  style={{ width: 30, height: 30, borderRadius: '50%', marginRight: 10 }}
                />
                {posts[0].author['first_name']} {posts[0].author['last_name']}
              </div>
            </div>
            <React.Fragment>
              <div className='row'>
                <div className='col-12'>
                  <h1 style={{ fontWeight: 'bold', width: '60%' }}>{posts[0].title}</h1>
                  {this.articlesRead < 6 ? (
                    <React.Fragment>
                      {Parser(posts[0].content)}
                    </React.Fragment>
                  ) : (
                      <div style={{ position: 'relative' }}>
                        <div>
                          {/* trimHtml is used to limit the content user is able to see. Even if
                          the user changes the visiblity of the div in the css properties manually 
                          in the inspector, the content will still be cut off.
                          */}
                          {Parser(trimHtml(posts[0].content, { limit: 200 }).html)}
                        </div>
                        <div style={styles.contentWrapper}>
                          <div style={{ textAlign: 'center', width: '50%' }}>
                            <h2 style={{ fontWeight: 'bold' }}>
                              Support independent journalism.
                            </h2>
                            <p>
                              This content is exclusive to subscribers.
                              Stay close to innovation in Asia by subscribing at just $0.27 per day.
                          </p>
                            <button style={styles.button}>
                              Learn More
                          </button>
                          </div>
                        </div>
                      </div>
                    )}

                </div>
              </div>
            </React.Fragment>
          </React.Fragment>
        ) : null}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  articles: state.Articles,
  cookies: ownProps.cookies,
});

const mapDispatchToProps = {
  getArticle,
};

export default connect(mapStateToProps, mapDispatchToProps)(Article);
