export default ({
  contentWrapper: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    backgroundColor: '#FFFFFF',
    background: 'linear-gradient(0deg, rgba(255, 255, 255, 1) 65%, rgba(255, 255, 255, 0))',
  },
  button: {
    borderRadius: 50,
    width: '55%',
    height: 35,
    backgroundColor: 'red',
    color: 'white',
    border: 'none',
    fontWeight: 'bold',
  },
});