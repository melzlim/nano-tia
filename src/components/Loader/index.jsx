import React from 'react';
import PropTypes from 'prop-types';

class Loader extends React.Component {
  static propTypes = {
    marginBottom: PropTypes.number,
    style: PropTypes.shape({}),
    noText: PropTypes.bool,
  }

  static defaultProps = {
    marginBottom: 20,
    style: {},
    noText: false,
  }

  render() {
    const styles = Object.assign({}, {
      marginBottom: this.props.marginBottom,
      fontFamily: 'Avenir',
    }, this.props.style);

    return (
      <div
        className="text-center"
        style={styles}
      >
        {this.props.noText === false ? <h3>Loading...</h3> : null}
        <br />
        <i className="fa fa-spin fa-2x fa-refresh" />
      </div>
    );
  }
}

export default Loader;