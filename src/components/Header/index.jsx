import React from 'react';
import logo from './assets/logo.png';

class Header extends React.Component {

  render() {

    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <img
          style={{ width: 150, height: 50 }}
          className="navbar-brand"
          src={logo} alt='logo'
          href="/" />
      </nav>
    );
  }
}


export default Header;
