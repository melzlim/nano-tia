import axios from 'axios';

export function GetArticles() {
  return axios.get('https://www.techinasia.com/wp-json/techinasia/2.0/posts');
}

export function GetArticle() {
  return axios.get('https://www.techinasia.com/wp-json/techinasia/2.0/posts/google-temasek-digital-economy-report-2018');
}